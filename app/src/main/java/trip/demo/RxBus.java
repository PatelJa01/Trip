package trip.demo;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Sony on 5/6/2018.
 */

public class RxBus {

    private static RxBus rxBus;

    public static RxBus getRxBus() {
        if (rxBus == null)
            rxBus = new RxBus();
        return rxBus;
    }

    private RxBus() {
    }

    private PublishSubject<Object> bus = PublishSubject.create();

    public void send(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObservable() {
        return bus;
    }
}
