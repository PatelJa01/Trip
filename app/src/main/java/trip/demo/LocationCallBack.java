package trip.demo;

import android.util.Log;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

/**
 * Created by Sony on 5/6/2018.
 */

public class LocationCallBack extends LocationCallback {

    @Override
    public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        Log.d("LocationResponseCheck",locationResult.toString());
        RxBus.getRxBus().send(locationResult);
    }

    @Override
    public void onLocationAvailability(LocationAvailability locationAvailability) {
        super.onLocationAvailability(locationAvailability);
    }
}
