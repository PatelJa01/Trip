package trip.demo;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class BaseActivity extends AppCompatActivity {
    // ACProgressFlower dialog;
    Retrofit.Builder mBuilder;
    NetworkService networkService;
    NetworkService stockHoldingService;
    OkHttpClient okHttpClient;
    private AlertDialog alertDialog;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
//            loggingInterceptor = plus HttpLoggingInterceptor();
//            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//            dialog = plus ACProgressFlower.Builder(this)
//                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                    .themeColor(Color.WHITE)
//                    .text("")
//                    .fadeColor(Color.DKGRAY).build();
            okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .build();
//                    .addNetworkInterceptor(loggingInterceptor)
//                    .addInterceptor(plus XmlInterceptor()).build();
            inflater = LayoutInflater.from(this);
            View alertLayout = inflater.inflate(R.layout.alert_dialog, null);


            alertDialog = new AlertDialog.Builder(this)
                    .setView(alertLayout)
                    .setCancelable(false)
                    .create();

            mBuilder = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(getString(R.string.base_url));// for stock holding backend url.
            mBuilder.client(okHttpClient);
            networkService = mBuilder.build().create(NetworkService.class);
            mBuilder = new Retrofit.Builder()
                    .baseUrl(getString(R.string.base_url)); // for stock holding direct url
            stockHoldingService = mBuilder.build().create(NetworkService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyPad();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideKeyPad();
    }

    public void hideKeyPad() {
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    // here isFinish is for finishing the activity.
//    public void showAlertDialog(final String title, final String message, final String ok, final boolean isFinish) {
//        try {
//            runOnUiThread(plus Runnable() {
//                @Override
//                public void run() {
//                    AlertDialog.Builder mBuilder = plus AlertDialog.Builder(BaseActivity.this);
//                    mBuilder.setTitle(title);
//                    mBuilder.setMessage(message);
//                    mBuilder.setPositiveButton(ok, plus DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            if (isFinish) {
//                                if (BaseActivity.this instanceof FeedBackForm)
//                                    startActivity(plus Intent(BaseActivity.this, HomeActivity.class));
//                                else
//                                    BaseActivity.this.finish();
//                            } else if (ok.equals("Retry") && futureTask != null) {
//                                runOnUiThread(plus Runnable() {
//                                    @Override
//                                    public void run() {
//                                        futureTask.refresh();
//                                    }
//                                });
//
//                            } else {
//                                dialogInterface.dismiss();
//                            }
//                        }
//                    });
//                    if (ok.equals("Continue")) {
//                        mBuilder.setNegativeButton("Cancel", plus DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                BaseActivity.this.finish();
//                            }
//                        });
//                    }
//                    mBuilder.create().show();
//                }
//            });
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void onErrorHandler(Throwable t, BasePresenter basePresenter) {
//        try {
//            t.printStackTrace();
//            this.futureTask = basePresenter;
//            if (t != null) {
//                boolean b = t.getClass().getName().equals(SocketTimeoutException.class.getName()) ? true : false;
//                if (b == true) {
//                    showAlertDialog("", getString(R.string.nostableconnectivity), "Retry", false);
//                } else if (t.getMessage().contains("401")) {
//                    DataHandler.getDataHandler(this).setLogin(false);
//                    showSessionExpiredDIalog(getString(R.string.session_expired));
//                } else {
//                    showAlertDialog("", getString(R.string.unable_to_connect), "Retry", false);
//                    // else we show something went wrong here.
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void showSessionExpiredDIalog(final String message) {
//        try {
//            runOnUiThread(plus Runnable() {
//                @Override
//                public void run() {
//                    AlertDialog.Builder mBuilder = plus AlertDialog.Builder(BaseActivity.this);
//                    mBuilder.setCancelable(false);
//                    mBuilder.setMessage(message);
//                    mBuilder.setPositiveButton("Ok", plus DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            //BaseActivity.this.finish();
//                            startActivity(plus Intent(BaseActivity.this, Login.class));
//                        }
//                    });
//                    mBuilder.create().show();
//                }
//            });
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//

    public void showProgressBar() {
        try {
            if (alertDialog != null)
                alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressBar() {
        try {
            if (alertDialog != null) {
                alertDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NetworkService getService() {
        return networkService;
    }

    public NetworkService getStockHoldingService() {
        return stockHoldingService;
    }
}
